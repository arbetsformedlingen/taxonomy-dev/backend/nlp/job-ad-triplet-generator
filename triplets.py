import json
import os
import random
import csv


class Triplets:
    
    def __init__(self, target_filename, dataset_filename):
        """Loads the data from the jsonl file to a list, 

        creates class variables of target filename and datset filename."""
        
        self._target_filename = target_filename
        self._dataset_filename = dataset_filename
        self._job_ads = []

    def header(self, filename):
        """Writes header to csv file."""

        with open(filename, 'w', newline='', encoding='utf-8') as fhand:
            data = csv.writer(fhand, delimiter=',')
            data.writerow(['anchor', 'positive', 'negative', 'positive-similarity',
            'negative-similarity', 'anchor-title', 'positive-title', 'negative-title', 
            'anchor-keywords', 'positive-keywords', 'negative-keywords'])

    def _read_data(self, start, num_of_lines=100000):
        """Reads n number of lines from json_file. 100 000 is default.
        
        Saves position in file so that the same lines are not read twice.
        If reaches the end of the, will start over from the beginning.

        Args:
            start: The bytes from where the reading should start.
            num_of_lines: The number of lines which should be read from the file."""

        self._job_ads = []
        n = 0
        size = os.path.getsize(self._dataset_filename)

        with open(self._dataset_filename, 'r', encoding='utf-8') as json_file:
            json_file.seek(start)
            while n < num_of_lines:
                self._job_ads.append(json_file.readline())
                
                if json_file.tell() == size:
                    json_file.seek(0)

                n += 1

            pos = json_file.tell()

        return pos

    def _extract_ads(self, json_str):
        """Extracts and formats job ads from the dataset.
        Extracts keywords, both /extracted/ and /enriched/, a unique ID, and the entire ad from 
        each job ad in the dataset.
        
        The keywords are added to a set.
        
        Args:
            json_str: A json string from the dataset, which is a jsonl file.
        
        Returns: 
                A tuple containing the job ad's keywords, ID and json string."""

        job_ad = json.loads(json_str)
        
        extracted = job_ad["keywords"]["extracted"]
        keywords = set(extracted["occupation"] + extracted["skill"]) 
        
        try:
            enriched = job_ad["keywords"]["enriched"]
            keywords = keywords.union(set(enriched["occupation"] + enriched["skill"]))
        except KeyError:
            pass

        return keywords, job_ad["id"], job_ad
        
    def _jaccard(self, set1, set2):
        """Returns the Jaccard index of two sets. A mesurement of how similar two sets are.
        
        Args:
            set1: A set of keywords from a job ad.
            set2: A set of keywords from a different job ad.
        Returns:
            The Jaccard index of set1 and set2."""

        return len(set1.intersection(set2)) / len(set1.union(set2))

    def _sample(self, k, min_keywords):
        """Creates a random sample of job ads of length k.
        
        Using random.sample(), creates a random sample out of the jobad dataset.
        Ensures that the first ad of the sample has as many keywords as specified in min_keywords.
        If the first job ad has to few a new random sample is procured.
        
        Args:
            k: The size of the sample.
            min_keywords: The minimum amount of keywords the first ad of the sample has to contain.
        Return:
            A tuple containing the extracted version of the first job ad and the rest job ads as elements."""
        
        sample = random.sample(self._job_ads, k)
        while len(self._extract_ads(sample[0])[0]) < min_keywords:
            sample = random.sample(self._job_ads, k)

        assert len(self._extract_ads(sample[0])[0]) >= min_keywords

        return self._extract_ads(sample[0]), sample[1:]

    def _save_triplet(self, anchor, neg_example, pos_example):
        """Saves triplets to csv file.

        The csv file has the columns:
            anchor: The id of the job ad used as anchor.
            positive: The id of the job ad used as positive example.
            negative: The id of the job ad used as negative example.
            positive-similarity: The jaccard index value of the positive example.
            negative-similarity: The jaccard index value of the negative example.
            anchor-title: The title of the anchor ad.
            positive-title: The title of the positive example ad.
            negative-title: The title of the negative example ad.
            anchor-keywords: The keywords of the anchor ad.
            positive-keywords: The keywords of the positive ad.
            negative-keywords: The keywords of the negative ad.

        Args:
            anchor: A tuple containing the elements: keywords of anchor job ad, id of anchor job ad, entire anchor job ad.
            neg_example: A tuple containing the elements: A tuple of keywords 
                of negative example job ad, id of negative example job ad, entire negative example job ad, and jaccard index value.
            pos_example: A tuple containing the elements: A tuple of keywords 
                of negative example job ad, id of negative example job ad, entire negative example job ad, and jaccard index value."""

        anchor_id = anchor[1]
        pos_id = pos_example[0][1]
        neg_id = neg_example[0][1]
        pos_sim = str(pos_example[1])
        neg_sim = str(neg_example[1])
        anchor_title = anchor[2]["headline"]
        pos_title = pos_example[0][2]["headline"]
        neg_title = neg_example[0][2]["headline"]
        anchor_keywords = anchor[0]
        pos_keywords = pos_example[0][0]
        neg_keywords = neg_example[0][0]

        with open(self._target_filename, 'a', newline='', encoding='utf-8') as fhand:
            data = csv.writer(fhand, delimiter=',')
            data.writerow([
                anchor_id, pos_id, neg_id, pos_sim, neg_sim, anchor_title, 
                pos_title, neg_title, anchor_keywords, pos_keywords, neg_keywords
            ])

    def triplets(self, min_keywords, k, neg_limit=0.3, pos_limit=0.6):
        """Creates triplets for SBERT training data.

        Gets a random sample of job ads of length k from the dataset. Chooses one of the job
        ads as anchor. Iterates over the sample. 
        For each sample, gets the job ad's keywords and calculates the Jaccard index.
        If the Jaccard index is more than the positive limit, saves as positive example.
        If the Jaccard index is less than the negative limit, saves as negative example.
        When both positive and negative examples have been found, 
        saves anchor, negative example, and positive example to file using _save_triplet().
        
        Args
            min_keywords: An integer specifying the minimum amount of keyword a job ad has to contain to be considered.
            k: The size of the random sample. Default is 1000.
            neg_limit: The upper limit of Jaccard index value for being used as negative example. Default is 0.3.
            pos_limit: The lower limit of Jaccard index value for being used as positive example. Default is 0.6."""

        anchor, ads = self._sample(k, min_keywords)
        neg_example = None
        pos_example = None

        for ad in ads:
            example = self._extract_ads(ad)
                
            if len(example[0]) >= min_keywords:
                jaccard = self._jaccard(anchor[0], example[0])

                if jaccard <= neg_limit:
                    neg_example = example, jaccard
                elif jaccard >= pos_limit:
                    pos_example = example, jaccard
                
            if neg_example is not None and pos_example is not None:
                self._save_triplet(anchor, neg_example, pos_example, min_keywords, k)
                neg_example, pos_example = None, None


def main(target_filename, dataset_filename, n=10000, k=1000, min_keywords=3):
    """Driver code for class Triplets.
    
    Iterates n times. For every hundred iteration, reads data from dataset with Triplets.read_data().
    Iterates over the read data, with Triplets.triplets(), creates triplets out of job ads.

    Args:
        target_filename: A csv file to which the triplets get written.
        dataset_filename: A jsonl-file of historical job ads.
        n: The number of iterations. Default is 10000.
        k: The size of the random sample.
        min_keywords: The minimum amount of keywords a job ad must have to be considered. Default is 3.
    """

    triplets = Triplets(target_filename, dataset_filename)
    triplets.header(target_filename)
    pos = 0

    for i in range(n):

        if i % 100 == 0:
            pos = triplets._read_data(pos)

        triplets.triplets(min_keywords, k)

        if i % 1000 == 0:
            print(f'{i} iterationer')


main(target_filename='', dataset_filename='')
